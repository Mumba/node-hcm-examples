/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {WampChallenge, WampClient, WampClientOptions} from 'mumba-wamp';
import Logger = require('bunyan');

/**
 * Create a WampClient instance.
 *
 * @param {WampClientOptions} options
 * @param {Logger} logger
 * @returns {WampClient}
 */
export function createWampClient(options: WampClientOptions, logger: Logger): WampClient {
	const wampClient = new WampClient(createWampOptions(options));

	wampClient.onOpen.subscribe(
		(session) => logger.debug({
			session_id: session.id
		}, 'Wamp:opened')
	);

	wampClient.onClose.subscribe(
		(details) => logger.debug({
			details,
			url: wampClient.getOptions().url
		}, 'Wamp:closed')
	);

	wampClient.onRegister.subscribe(
		(resp) => logger.debug('Wamp:registered `%s`.', resp.procedure)
	);

	wampClient.onSubscribe.subscribe(
		(resp) => logger.debug('Wamp:subscribed `%s`.', resp.procedure)
	);

	wampClient.onError.subscribe(
		(err) => logger.error({
			err
		}, err.message ? err.message : err)
	);

	logger.debug('Wamp:openConnection');

	wampClient.openConnection();

	return wampClient;
}

/**
 * Create the options for the WAMP client, injecting the appropriate challenge function.
 */
function createWampOptions(options: WampClientOptions): any {
	const wampchallenge = new WampChallenge();

	options['onchallenge'] = wampchallenge.createWampCra(options['secret']);

	return options;
}
