/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

export interface PayslipComponent {
	type?: string;
	typeLabel?: string;
	code?: string;
	codeLabel?: string;
	amount?: string;
	hours?: string;
	rate?: string;
	filter?: string;
}

export interface Payslip {
	payRun?: string;
	gross?: string;
	tax?: string;
	nett?: string;
	super?: string;
	addressType?: string;
	address?: string;
	suburb?: string;
	state?: string;
	postcode?: string;
	company?: string;
	abn?: string;
	payDate?: string;
	payFromDate?: string;
	payToDate?: string;
	timezone?: string;
	components?: PayslipComponent[];
	salaryClass?: string;
	salaryAward?: string;
	annualLeave?: string;
}
