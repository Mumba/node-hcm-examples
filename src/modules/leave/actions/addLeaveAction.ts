/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {NAMESPACE} from '../../../etc/config';
import {UserService} from '../../users/UserService';
import {Leave} from '../models/Leave';

export interface LeaveToAdd extends Leave {
	comment: string;
}

export function addLeaveAction(userService: UserService) {

	/**
	 * Add leave using your API.
	 */
	async function addLeave(values: LeaveToAdd): Promise<LeaveToAdd> {
		// user.uid will be the username for your API.
		const user = await userService.getUserFromCall(values.createdBy);

		// Invoke your API and return your data here.
		return {} as LeaveToAdd;
	}

	return {
		name: `${NAMESPACE}.leave.add`,

		callback: async function (args: LeaveToAdd[], kwargs: any): Promise<LeaveToAdd[]> {

			return Promise.all(args.map(addLeave));
		}
	};
}
