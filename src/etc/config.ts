/**
 * Default configuration for testing.
 *
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

// Remember, path is relative to /dist/bin/start.js
const NAME = require('../../../package.json').name;
const VERSION = require('../../../package.json').version;
const bunyan = require('bunyan');

const defaultConfig = {
	logger: {
		name: NAME + '@' + VERSION,
		serializers: bunyan.stdSerializers,
		streams: [
			{
				level: 'debug',
				stream: process.stdout
			}
		]
	},

	wamp: {
		url: 'ws://mumba__crossbar:8080',
		realm: 'test-realm',
		authid: '1',
		authmethods: ['wampcra'],
		secret: '*password'
	}
};

export default defaultConfig;

// Change the NAMESPACE as directed by Mumba.
export const NAMESPACE = 'COMPANY';
