/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {NAMESPACE} from '../../../etc/config';
import {Leave} from '../models/Leave';
import {UserService} from '../../users/UserService';

export interface GetLeaveFilter {
	ids?: any[];

	createdBy?: number;
	approver?: number;
	status?: string | string[]; // enum 'pending', 'approved', 'refused', 'cancelling', 'cancelled'
	before?: Date | number;
	after?: Date | number;
	codeId?: number;
	ordering?: string; // 'future' | 'past'

	offset?: number;
	limit?: number;

	addCode?: boolean; // must include `leaveCode` property in the leave request
	addComments?: boolean; // must include `leaveComments` property in the leave request
}

export function getLeaveAction(userService: UserService) {

	/**
	 * Get a list of leave request from your API, filtering by username if available.
	 */
	async function getLeave(username: string, options: GetLeaveFilter): Promise<Leave[]> {
		// Invoke your API and return your data here.
		return [{} as Leave];
	}

	return {
		name: `${NAMESPACE}.leave.get`,

		callback: async function (args: string[], kwargs: GetLeaveFilter): Promise<Leave[]> {
			const user = await userService.getUserFromCall(kwargs.createdBy);

			return getLeave(user.uid, kwargs);
		}
	};
}
