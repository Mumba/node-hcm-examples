/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {NAMESPACE} from '../../../etc/config';
import {UserService} from '../../users/UserService';
import {LeaveBalances} from '../models/LeaveBalances';

export interface LeaveBalancesFilter {
	userId: number;
	at: number;
}

export function getBalancesAction(userService: UserService) {

	/**
	 * Get a list of leave balances using your API, for a user relative a specified date.
	 *
	 * @example
	 *
	 * // Returns a dictionary of 'codeId':'value'
	 * // leave balances in hours. For example:
	 * {
	 *   annual: 123.45,
	 *   sick: 78.9
	 * };
	 */
	async function getBalances(username: string, date: Date): Promise<LeaveBalances> {
		// Invoke your API and return your data here.
		return {} as LeaveBalances;
	}

	return {
		name: `${NAMESPACE}.leave.balances.get`,

		callback: async function (args: any[], kwargs: LeaveBalancesFilter,): Promise<LeaveBalances> {
			const user = await userService.getUserFromCall(kwargs.userId);

			// kwargs.at is any single value that can be used in the JavaScript Date object constructor.
			return getBalances(user.uid, new Date(kwargs.at));
		}
	};
}
