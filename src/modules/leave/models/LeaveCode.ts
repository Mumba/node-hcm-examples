/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

// Note that dates come back from the API as strings in UTC.
export interface LeaveCode {
	id?: number | string;
	label?: string;
	enabled?: boolean;
	createdAt?: string;
	updatedAt?: string;
	deletedAt?: string;
}
