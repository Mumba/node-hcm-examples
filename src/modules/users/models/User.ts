/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

export interface User {
	id?: number;
	uid?: string;
	firstName?: string;
	lastName?: string;
}
