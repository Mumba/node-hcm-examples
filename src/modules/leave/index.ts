/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {WampClient} from 'mumba-wamp';
import {getCodesAction} from './actions/getCodesAction';
import {UserService} from '../users/UserService';
import {addLeaveAction} from './actions/addLeaveAction';
import {getBalancesAction} from './actions/getBalancesAction';
import {getLeaveAction} from './actions/getLeaveAction';

/**
 * This function is used to wire your pay API's to our WAMP server.
 */
export function registerLeaveModule(wampClient: WampClient, userService: UserService) {
	const actions = [
		addLeaveAction(userService),
		getBalancesAction(userService),
		getCodesAction(userService),
		getLeaveAction(userService)
	];

	actions.forEach(action => wampClient.register(action.name, action.callback))
}
