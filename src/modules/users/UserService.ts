/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {WampClient, WampSession} from 'mumba-wamp';
import {User} from './models/User';

export class UserService {
	constructor(private wampClient: WampClient, private wampSession: WampSession) {
	}

	public async getUserFromCall(caller: number) {
		const session = await this.wampSession.get(caller);

		return this.getUser(session.caller);
	}

	private getUser(userId: number): Promise<User> {
		return this.wampClient.call('mumba.users.get', [userId])
			.then((results: User[]) => results[0]);
	}
}
