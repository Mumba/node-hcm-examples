/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {WampClient} from 'mumba-wamp';
import {NAMESPACE} from '../../../etc/config';
import {UserService} from '../../users/UserService';
import {Payslip} from '../models/Payslip';

export interface GetPayslipsFilter {
	userId?: number;
	offset?: number;
	limit?: number;
}

/**
 * Get a list of payslips.
 */
export function getPayslipsAction(wampClient: WampClient, userService: UserService) {

	/**
	 * Get one full payslip using your API.
	 */
	async function getOneByPayRun(username: string, payrun: string): Promise<Payslip> {
		// Invoke your API and return your data here.
		return {} as Payslip;
	}

	/**
	 * Get a list of abridged payslip details for a user from your API.
	 */
	async function getAllByUsername(username: string, limit: number, offset: number): Promise<Payslip[]> {
		// Invoke your API and return your data here.
		return [{} as Payslip];
	}

	return {
		name: `${NAMESPACE}.payslips.get`,

		callback: async function getPayslips(args: string[], kwargs: GetPayslipsFilter, details: any): Promise<Payslip[]> {
			const user = await userService.getUserFromCall(details.caller);

			// If args is populated, it's an array of payruns.
			if (args.length) {
				return Promise.all(args.map(arg => getOneByPayRun(user.uid, arg)));
			}

			return getAllByUsername(user.uid, kwargs.limit, kwargs.offset);
		}
	}
}
