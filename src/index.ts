/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import defaultConfig from './etc/config';
import {registerPayModule} from './modules/pay';
import {createWampClient} from './modules/wamp';
import {createLogger} from './modules/logger';
import {createUserService} from './modules/users';
import {registerLeaveModule} from './modules/leave';

const logger = createLogger(defaultConfig.logger as any);
const wampClient = createWampClient(defaultConfig.wamp as any, logger);
const userService = createUserService(wampClient);

registerPayModule(wampClient, userService);
registerLeaveModule(wampClient, userService);
