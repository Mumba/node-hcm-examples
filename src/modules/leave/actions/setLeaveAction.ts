/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {NAMESPACE} from '../../../etc/config';
import {Leave} from '../models/Leave';
import {UserService} from '../../users/UserService';

export interface LeaveToSet extends Leave {
	comment: string;
}

export function setLeaveAction(userService: UserService) {

	/**
	 * Get a list of leave request from your API, filtering by username if available.
	 */
	async function setLeave(username: string, values: LeaveToSet): Promise<LeaveToSet> {
		// Invoke your API and return your data here.
		return {} as LeaveToSet;
	}

	return {
		name: `${NAMESPACE}.leave.set`,

		callback: async function (args: LeaveToSet[], kwargs: any): Promise<LeaveToSet[]> {

			return Promise.all(args.map(async arg => {
				const user = await userService.getUserFromCall(arg.createdBy);

				return setLeave(user.uid, arg);
			}));
		}
	};
}
