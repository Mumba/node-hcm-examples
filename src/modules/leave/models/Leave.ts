/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Moment} from 'moment-timezone';
import {LeaveCode} from './LeaveCode';
import {LeaveComment} from './LeaveComment';

export interface Leave {
	id?: string;
	codeId?: number | string;
	status?: string; // enum 'pending', 'approved', 'refused', 'cancelling', 'cancelled'
	startsAt?: Date | number | string | Moment; // UTC
	endsAt?: Date | number | string | Moment;   // UTC
	timezone?: string;
	duration?: number;
	isFullDay?: boolean;
	approver1?: number;
	approver2?: number;
	createdBy?: number;
	updatedBy?: number;
	approvedBy?: number;
	approvedAt?: Date;
	cancellingAt?: Date;
	cancelledBy?: number;
	cancelledAt?: Date;
	createdAt?: Date;  // required: UTC
	updatedAt?: Date;
	deletedAt?: Date;

	leaveCode?: LeaveCode;
	leaveComments?: LeaveComment[];
}
