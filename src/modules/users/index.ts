/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {WampClient, WampSession} from 'mumba-wamp';
import {UserService} from './UserService';

export function createUserService(wampClient: WampClient) {
	const wampSession = new WampSession(wampClient);
	const userService = new UserService(wampClient, wampSession);

	return userService;
}
