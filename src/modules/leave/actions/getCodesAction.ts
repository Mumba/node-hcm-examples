/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {NAMESPACE} from '../../../etc/config';
import {UserService} from '../../users/UserService';
import {LeaveCode} from '../models/LeaveCode';

export interface LeaveCodeFilter {
	userId: number;
}

export function getCodesAction(userService: UserService) {

	/**
	 * Get a list of leave codes using your API, filtering by username if appropriate.
	 */
	async function getCodes(username: string): Promise<LeaveCode[]> {
		// Invoke your API and return your data here.
		return [{} as LeaveCode];
	}

	return {
		name: `${NAMESPACE}.leave.codes.get`,

		callback: async function (args: any[], kwargs: LeaveCodeFilter): Promise<LeaveCode[]> {
			const user = await userService.getUserFromCall(kwargs.userId);

			return getCodes(user.uid);
		}
	};
}
