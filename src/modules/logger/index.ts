/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */
import {LoggerOptions} from 'bunyan';
import Logger = require('bunyan');

export function createLogger(options: LoggerOptions) {
	const logger = new Logger(options);

	return logger;
}
