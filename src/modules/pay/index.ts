/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {WampClient} from 'mumba-wamp';
import {UserService} from '../users/UserService';
import {getPayslipsAction} from './actions/getPayslipsAction';

/**
 * This function is used to wire your pay API's to our WAMP server.
 */
export function registerPayModule(wampClient: WampClient, userService: UserService) {
	const actions = [
		getPayslipsAction(wampClient, userService)
	];

	actions.forEach(action => wampClient.register(action.name, action.callback))
}
