#--------------------------
#    Base Dependencies    #
#--------------------------
FROM node:8-alpine as base
RUN apk --no-cache add --update \
		python \
		openssl \
		libgcc \
		make \
		libstdc++ \
		g++ \
		ca-certificates \
		wget \
		tar \
		unzip \
		curl \
		unzip

#--------------------------
#   Mumba Dependencies    #
#--------------------------
FROM base AS mumba
ENV DOCKERIZE_VERSION v0.6.0
WORKDIR /usr/src
COPY . .
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz -O dockerize.tar.gz -q \
		&& tar -xzvf dockerize.tar.gz \
	&& npm install \
    && npm run compile

#--------------------------
#      Testing Node       #
#--------------------------
FROM mumba AS test
CMD ./dockerize \
		-wait tcp://rabbitmq:5672 \
		-wait http://mumba__crossbar:8080 \
    && npm run cover \
    # copy these out as artifacts for gitlab
    && cp -a /usr/src/coverage /artifacts \
    && cp -a /usr/src/reports /artifacts
