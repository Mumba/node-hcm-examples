/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {UserService} from '../../../../src/modules/users/UserService';
import {WampClient, WampSession} from 'mumba-wamp';

describe('UserService unit tests.', () => {
	let instance: UserService;

	beforeEach(() => {
		const wampClient = Object.create(WampClient.prototype);
		const wampSession = Object.create(WampSession.prototype);

		instance = new UserService(wampClient, wampSession);
	});

	it('should work', () => {
		// Write your test!
	});
});
