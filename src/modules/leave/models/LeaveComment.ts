/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

// Note that dates come back from the API as strings in UTC.
export interface LeaveComment {
	id?: string;
	leaveId?: number;
	status?: string;
	comment?: string;
	createdBy?: number;
	updatedBy?: number;
	createdAt?: string;
	updatedAt?: string;
	deletedAt?: string;
}
